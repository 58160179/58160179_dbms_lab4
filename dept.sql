CREATE VIEW dept AS
	SELECT Department.Dname,Dept_Locations.LocationName
	FROM Department JOIN Dept_Locations
	ON Department.Dnumber = Dept_Locations.Lnumber
	ORDER By Dept_Locations.LocationName;
