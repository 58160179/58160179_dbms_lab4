CREATE VIEW management AS
	SELECT Department.Dnumber,Department.Dname,Manager.Fname,Manager.Lname
	From Department JOIN Manager
	ON Department.Dnumber = Manager.Mnumber;

