CREATE VIEW SalePerOrder AS
	SELECT 
		orderNumber, SUM(quantityOrdered * priceEach) total 
	FROM
		orderdetails
	GROUP by orderNumber
	ORDER by total DESC;
